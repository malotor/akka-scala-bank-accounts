network:
	docker network create bank-accounts

build:
	docker-compose build

new-project:
	docker-compose run scala sbt new scala/hello-world.g8

bash:
	docker-compose run --publish 8080:8080 scala bash 
	# docker run -it --rm --network bank-accounts --volume $$(pwd):/usr/src/app scala bash

client:
	docker-compose run client bash 
	# docker run -it --rm --network bank-accounts --volume $$(pwd):/usr/src/app client bash
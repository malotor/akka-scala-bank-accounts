require 'httparty'
require 'json'

threads = []
30.times do |i|
  threads << Thread.new do
    body = {
      :from => '0001-USD', 
      :to => '0002-USD', 
      :amount => rand(1000..1500)
    }.to_json

    options = {
      body: body,
      headers: {
        "X-Api-Key" => "#{i}",
        "Content-Type" => "application/json"
      }
    }

    response = HTTParty.post("http://scala:8080/transfers", options)
    puts response.body
  end
end
threads.each(&:join)
scalaVersion := "2.13.3"

name := "bank-accounts"
organization := "com.manel"
version := "1.0"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"

libraryDependencies ++= Seq(
  	"com.typesafe.akka"           %% "akka-actor"       				% "2.6.12",
  	"com.typesafe.akka"           %% "akka-persistence" 				% "2.6.12",
  	"org.iq80.leveldb"            % "leveldb"           				% "0.7",
  	"org.fusesource.leveldbjni"   % "leveldbjni-all"    				% "1.8",
		"com.typesafe.akka" 					%% "akka-persistence-query" 	% "2.6.12",
		"com.typesafe.akka" 					%% "akka-http"                % "10.2.4",
		"com.typesafe.akka" 					%% "akka-http-spray-json"     % "10.2.4",
		"com.typesafe.akka" 					%% "akka-stream"              % "2.6.12",
		"ch.qos.logback"    					% "logback-classic"           % "1.2.3",
		"com.typesafe.akka" 					%% "akka-slf4j" 							% "2.6.12",
		"com.typesafe.akka" 					%% "akka-http"                % "10.2.4",
    "com.typesafe.akka" 					%% "akka-http-spray-json"     % "10.2.4",
    "com.typesafe.akka" 					%% "akka-stream"              % "2.6.12"
)

scalacOptions ++= Seq(
  "-language:postfixOps"
)
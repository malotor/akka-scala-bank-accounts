package com.manel.bank_accounts.section4

import akka.actor.{ Actor, ActorRef, ActorSystem, Props , FSM}
import com.manel.bank_accounts.section3.AdvancedAccount
import com.manel.bank_accounts.CommonActor

object WireTransfer {
  // Commands
  case class Transfer(from: ActorRef, to: ActorRef, amount: Int)

  // Domain Events
	sealed trait DomainEvent
  case class TransferDone(from: ActorRef,to: ActorRef, amount: Int) extends DomainEvent
  case class TransferFailed(from: ActorRef,to: ActorRef, amount: Int, reason: String) extends DomainEvent

  sealed trait State
  object Initial extends State
  object AwaitFrom extends State
  object AwaitTo extends State
  object Done extends State

  sealed trait Data
  case object UninitializedWireTransferData extends Data
  case class InitialisedWireTransferData(from: ActorRef, to: ActorRef, amount: Int, client: ActorRef) extends Data
}

class WireTransfer extends FSM[WireTransfer.State, WireTransfer.Data] with CommonActor {
  import AdvancedAccount._
  import WireTransfer._

  startWith(Initial, UninitializedWireTransferData)

  when(Initial) {
    case Event(Transfer(from, to, amount), UninitializedWireTransferData) =>
      from ! Operation(amount, DR)
      goto(AwaitFrom) using InitialisedWireTransferData(from, to, amount, sender())
  }

  when(AwaitFrom) {
    case Event(AcceptedTransaction(_,_,_,_), InitialisedWireTransferData(_, to, amount, _)) =>
      to ! Operation(amount, CR)
      goto(AwaitTo)
    case Event(RejectedTransaction(account,_,_,reason,currentBalance), InitialisedWireTransferData(account_from, account_to, amount, client)) =>
      client ! WireTransfer.TransferFailed(account_from, account_to, amount, s"${account.path.name} rejected operation '$reason'")
      goto(Done)
      stop()
  }

  when(AwaitTo) {
    case Event(AcceptedTransaction(_,_,_,_), InitialisedWireTransferData(account_from, account_to, amount, client)) =>
      client ! WireTransfer.TransferDone(account_from, account_to, amount)
      goto(Done)
      stop()
    case Event(RejectedTransaction(account,_,_,reason,currentBalance), InitialisedWireTransferData(account_from, account_to, amount, client)) =>
      client ! WireTransfer.TransferFailed(account_from, account_to, amount,s"${account.path.name} rejected operation $reason")
      goto(Done)
      stop()
  }
}

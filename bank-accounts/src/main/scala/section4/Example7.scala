package com.manel.bank_accounts.section4

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.section3.AdvancedAccount
import com.manel.bank_accounts.CommonApp

object Example7 extends App with CommonApp {
  import WireTransfer._
  import PremiumOperator._

  val system = ActorSystem("Bank-Accounts")

  val accountA = system.actorOf(AdvancedAccount.props("0001-USD"), name = "0001-USD")
  val accountB = system.actorOf(AdvancedAccount.props("0002-USD"), name = "0002-USD")
  val accountC = system.actorOf(AdvancedAccount.props("0003-USD", 2500), name = "0003-USD")

  val operator = system.actorOf(PremiumOperator.props, "operator")

  operator ! MakeTransaction(accountA, accountB, 1500)
  Thread.sleep(1000)
  operator ! MakeTransaction(accountC, accountA, 1000)
  Thread.sleep(1000)
  operator ! MakeTransaction(accountC, accountA, 1000)

  Thread.sleep(10000)
  system.terminate()
}
package com.manel.bank_accounts.section4

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.section3.AdvancedAccount
import com.manel.bank_accounts.CommonApp

object Example8 extends App with CommonApp {
  import PremiumSupervisor._

  val system = ActorSystem("Bank-Accounts")

  system.actorOf(AdvancedAccount.props("0001-USD", 1000), name = "0001-USD")
  system.actorOf(AdvancedAccount.props("0002-USD", 500), name = "0002-USD")

  val supervisor = system.actorOf(PremiumSupervisor.props, "supervisor")

  supervisor ! MakeWireTransfer("0001-USD", "0002-USD", 1000)

  Thread.sleep(10000)
  system.terminate()
}
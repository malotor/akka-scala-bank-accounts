package com.manel.bank_accounts.section1

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.CommonApp

// Dispatchers
// https://blog.knoldus.com/akka-dispatcher-all-that-you-need-to-know/
object Example2 extends App with CommonApp {
  import Account._

  val system = ActorSystem("Bank-Accounts")

  // val accounts: Seq[ActorRef] = ( 1 to 10 ).map( accountId =>
  //   system.actorOf(Props[Account](), s"000$accountId-USD")
  // )

  // accounts.foreach( account => {
  //   account ! Operation(1000,CR)
  //   account ! Operation(200,DR)
  //   account ! Operation(200,DR)
  //   account ! Operation(500,CR)
  // })

  val dinamic_accounts: Seq[ActorRef] = ( 1 to 10 ).map( accountId =>
    system.actorOf(Props[Account]().withDispatcher("dinamic-thread"), s"D_000$accountId-USD")
  )

  dinamic_accounts.foreach( account => {
    account ! Operation(1000,CR)
    account ! Operation(200,DR)
    account ! Operation(200,DR)
    account ! Operation(500,CR)
  })

  // val fixedAccounts: Seq[ActorRef] = ( 1 to 10 ).map( accountId =>
  //   system.actorOf(Props[Account]().withDispatcher("fixed-thread"), s"F_000$accountId-EUR")
  // )

  // fixedAccounts.foreach( account => {
  //   account ! Operation(1000,CR)
  //   account ! Operation(200,DR)
  //   account ! Operation(200,DR)
  //   account ! Operation(500,CR)
  // })

  // val pinnedAccounts: Seq[ActorRef] = ( 1 to 10 ).map( accountId =>
  //   system.actorOf(Props[Account]().withDispatcher("pinned-thread-pool"), s"P_000$accountId-EUR")
  // )

  // pinnedAccounts.foreach( account => {
  //   account ! Operation(1000,CR)
  //   account ! Operation(200,DR)
  //   account ! Operation(200,DR)
  //   account ! Operation(500,CR)
  // })

  Thread.sleep(10000)
  system.terminate()
}
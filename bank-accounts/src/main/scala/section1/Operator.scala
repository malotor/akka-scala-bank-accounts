package com.manel.bank_accounts.section1

import akka.actor.{ Actor, Props, ActorRef }
import com.manel.bank_accounts.CommonActor

object Operator {
  trait Commands
  case class Transaction(account: ActorRef, amount: Int) extends Commands

  def props = Props[Operator]
}

class Operator extends Actor with CommonActor {
  import Operator._
  import Account._

  def receive = {
    case Transaction(account, amount) if amount > 0 => {
      printFullLog(s"Sending CR $amount")
      account ! Operation(amount, CR)
    }
    case Transaction(account, amount) => {
      printFullLog(s"Sending DR $amount")
      account ! Operation(amount.abs, DR)
    }
  }
}
package com.manel.bank_accounts.section1

import akka.actor.Actor
import com.manel.bank_accounts.CommonActor

// Companion object
// https://docs.scala-lang.org/overviews/scala-book/companion-objects.html
object Account {
	// Transaction Types
	sealed trait TransactionType
	case object CR extends TransactionType
	case object DR extends TransactionType

	// Messages
	case class Operation(amount: Int, `type`: TransactionType)
}

class Account extends Actor with CommonActor {
  import Account._

  // Internal state
  var balance: Int = 0

  // Receive messages
  def receive = {
    case Operation(amount, CR) => {
      Thread.sleep(1000)
      balance += amount
      printBalance
    }
    case Operation(amount, DR) => {
      Thread.sleep(1000)
      balance -= amount
      printBalance
    }
  }

  def printBalance = {
    printFullLog(s"Current balance: ${balance}")
  }
}
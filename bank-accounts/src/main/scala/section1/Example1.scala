package com.manel.bank_accounts.section1

import akka.actor.{ActorSystem, ActorRef , Props}
import com.manel.bank_accounts.CommonApp

object Example1 extends App with CommonApp {
  // Import static object
  import Account._

  // 1 - Create a new actor system
  // https://doc.akka.io/docs/akka/current/general/addressing.html
  val system = ActorSystem("Bank-Accounts")

  // 2 - Create an actor
  // ActorRef : akka://Bank-Accounts/user/0001-USD
  val account = system.actorOf(Props[Account], "0001-USD")

  // 3 - Send a message
  printLog("Sending operations ...")
  // Infix notation
  account ! Operation(1000,CR)
  account ! Operation(200,DR)
  account ! Operation(200,CR)
  account ! Operation(400,DR)
  printLog("Operations sended!")

  // 4 - Waiting for actors to finish
  Thread.sleep(10000)
  // 5 - This will coordinate all actors finish lifecyle
  system.terminate()
}
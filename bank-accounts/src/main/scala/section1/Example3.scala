package com.manel.bank_accounts.section1

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.CommonApp

object Example3 extends App with CommonApp {
  import Operator._
  import Account._

  val system = ActorSystem("Bank-Accounts")
  
  val account = system.actorOf(Props[Account], "0001-USD")

  val mike = system.actorOf(Operator.props, "Mike")
  val carol = system.actorOf(Operator.props, "Carol")

  mike ! Transaction(account, 1000)
  carol ! Transaction(account, 500)
  mike ! Transaction(account, 400)
  carol ! Transaction(account, -1600)
  // Thread.sleep(100)
  mike ! Transaction(account, 400)
  carol ! Transaction(account, -600)
  mike ! Transaction(account, -250)
  carol ! Transaction(account, 100)

  Thread.sleep(10000)
  system.terminate()
}
package com.manel.bank_accounts.section5

import akka.actor.{ Actor, Props, ActorRef, Terminated, ActorLogging }
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, Router }
import com.manel.bank_accounts.CommonActor

import scala.concurrent.Await
import akka.util.Timeout
import scala.concurrent.duration._

object PremiumSupervisor {
  trait Command
  case class MakeWireTransfer(from: String, to: String, amount: Int) extends Command
  def props = Props[PremiumSupervisor]
}

class PremiumSupervisor extends Actor with CommonActor {
  import PremiumSupervisor._
  import PremiumOperator._

  var router = {
    val operators = Vector.tabulate(3) { n => {
        val operator = createNewOperator(n)
        ActorRefRoutee(operator)
      }
    }
    Router(RoundRobinRoutingLogic(), operators)
  }

  def receive = {
    case MakeWireTransfer(from, to, amount) =>
      implicit val resolveTimeout = Timeout(5 seconds)

      val account_from = Await.result(context.actorSelection(s"/user/${from}").resolveOne(), resolveTimeout.duration)
      val account_to = Await.result(context.actorSelection(s"/user/${to}").resolveOne(), resolveTimeout.duration)

      val transaction = MakeTransaction(account_from, account_to, amount)

      router.route(transaction, sender())
  }

  def createNewOperator(n: Int) = {
    context.actorOf(PremiumOperator.props, s"operator_$n")
  }
}
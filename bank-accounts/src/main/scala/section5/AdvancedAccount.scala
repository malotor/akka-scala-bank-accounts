
package com.manel.bank_accounts.section5

import akka.actor.{ Actor, ActorRef, ActorSystem, Props, FSM}
import scala.reflect._
import akka.persistence._
import akka.persistence.fsm._
import akka.persistence.fsm.PersistentFSM.FSMState
import com.manel.bank_accounts.CommonActor

object AdvancedAccount {
	sealed trait State extends FSMState

	case object Empty extends State {
		override def identifier = "Empty"
	}

	case object Active extends State {
		override def identifier = "Active"
	}

	sealed trait Data {
		val amount: Int
	}

	case object ZeroBalance extends Data {
		override val amount: Int = 0
	}
	case class Balance(override val amount: Int) extends Data
	
	// Transaction Types
	sealed trait TransactionType
	case object CR extends TransactionType
	case object DR extends TransactionType
	
	// Commands
	case class Operation(amount: Int, `type`: TransactionType)
	
	// Domain Events
	sealed trait DomainEvent
	case class AcceptedTransaction(account: ActorRef, amount: Int,`type`: TransactionType, newBalance: Int) extends DomainEvent
	case class RejectedTransaction(account: ActorRef, amount: Int,`type`: TransactionType, reason: String, currentBalance: Int) extends DomainEvent

	def props(accountId: String): Props = Props(new AdvancedAccount(accountId))
}

class AdvancedAccount(val accountId: String) extends PersistentFSM[AdvancedAccount.State, AdvancedAccount.Data, AdvancedAccount.DomainEvent] with CommonActor {
	import AdvancedAccount._
  	
	// Id
	override def persistenceId: String = accountId
	
	// Domain events Class
	override def domainEventClassTag: ClassTag[DomainEvent] = classTag[DomainEvent]

	// Apply events function
	override def applyEvent(evt: DomainEvent, currentData: Data): Data = {
		evt match {
			case AcceptedTransaction(_, amount, CR, _) =>
				printLog(s"APPLYING EVENT: ${evt}")
				val newAmount = currentData.amount + amount
				Balance(currentData.amount + amount)
			case AcceptedTransaction(_, amount, DR, _) =>
				printLog(s"APPLYING EVENT ${evt}")
				val newAmount = currentData.amount - amount
				if(newAmount > 0)
					Balance(newAmount)
				else
					ZeroBalance
			case RejectedTransaction(_, _, _, reason, balance) =>
				printLog(s"APPLYING EVENT ${evt}")
				currentData
		}
	}

	startWith(Empty, ZeroBalance)

	when(Empty){
		case Event(Operation(amount, CR), _) =>
			val event = AcceptedTransaction(self, amount, CR, amount)
			goto(Active) applying event replying event
		case Event(Operation(amount, DR), _) =>
			val event = RejectedTransaction(self, amount, DR, "Balance is Zero", 0)
			stay applying event replying event
	}

	when(Active){
		case Event(Operation(amount, CR), balance) =>
			val newBalance = balance.amount + amount
			val event = AcceptedTransaction(self, amount, CR, newBalance)
			printLog(s"ACCEPTED CR $amount - new balance $newBalance")
			stay applying event replying event
		case Event(Operation(amount, DR), balance) =>
			val newBalance = balance.amount - amount
			if(newBalance > 0){
				printLog(s"ACCEPTED DR $amount - new balance $newBalance")
				val event = AcceptedTransaction(self, amount, DR, newBalance)
				stay applying event replying event
			}
			else if(newBalance == 0){
				printLog(s"ACCEPTED DR $amount - new balance $newBalance")
				val event = AcceptedTransaction(self, amount, DR, newBalance)
				goto(Empty) applying event replying event
			}
			else {
				printLog(s"REJECTED DR $amount - new balance $newBalance")
				val event = RejectedTransaction(self, amount, DR, "Balance doesn't cover this operation", balance.amount)
				stay applying event replying event
			}
	}

	whenUnhandled {
    case Event(e, s) =>
      log.error("received unhandled request {} in state {}/{}", e, stateName, s)
      stay()
  }
}
package com.manel.bank_accounts.section5

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.CommonApp

object Example9 extends App with CommonApp {
  import AdvancedAccount._

  val system = ActorSystem("Bank-Accounts")
  
  val account = system.actorOf(AdvancedAccount.props("F0001-USD"), name = "F0001-USD")

  account ! Operation(1200, CR)
  account ! Operation(2000, CR)
  account ! Operation(8000, DR)
  
  Thread.sleep(5000)
  system.terminate()
}
package com.manel.bank_accounts.section5

import akka.stream.scaladsl.Source
import akka.stream.ActorMaterializer
import akka.actor.ActorSystem
import akka.persistence.query.{ PersistenceQuery, EventEnvelope}
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import com.manel.bank_accounts.CommonApp


object Reporter extends App with CommonApp {
  if (args.length == 0) {
    println("Account id required")
  }
  val accountId: String = args(0)

  val system = ActorSystem("persistent-query")

  implicit val mat = ActorMaterializer()(system)

  val queries = PersistenceQuery(system).readJournalFor[LeveldbReadJournal](
    LeveldbReadJournal.Identifier
  )

  val evts: Source[EventEnvelope, _] = queries.eventsByPersistenceId(accountId,  0, Long.MaxValue)
  evts.runForeach { evt => println(s"Event $evt")}

  Thread.sleep(1000)
  system.terminate()
}
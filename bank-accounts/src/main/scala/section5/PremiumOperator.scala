package com.manel.bank_accounts.section5

import akka.actor.{ Actor, Props, ActorRef, ActorLogging }
import scala.concurrent.duration._
import scala.concurrent._
import com.manel.bank_accounts.CommonActor

object PremiumOperator {
  trait Commands
  case class MakeTransaction(account_from: ActorRef, account_to: ActorRef, amount: Int) extends Commands

  def props = Props[PremiumOperator]
}

class PremiumOperator extends Actor with CommonActor {
  import PremiumOperator._
  import WireTransfer._
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  def receive = {
    case MakeTransaction(account_from,account_to, amount) => {
      val wire = context.system.actorOf(Props[WireTransfer])

      wire ! WireTransfer.Transfer(account_from, account_to, amount)
    }
    case TransferFailed(account_from, account_to, amount,reason) => {
      printLog(s"WireTransfer failed $reason. Scheduling new transfer!")
      scheduleNewTransfer(account_from, account_to, amount)
    }
    case TransferDone(account_from, account_to, amount) => {
      printLog(s"WireTransfer Done! From: ${account_from.path.name} To: ${account_to.path.name} Amount: $amount")
    }
  }

  private def scheduleNewTransfer(account_from: ActorRef, account_to: ActorRef, amount: Int) = {
    val newWireTransferCommand = MakeTransaction(account_from, account_to, amount)
    context.system.scheduler.scheduleOnce(1 seconds, self, newWireTransferCommand)
  }
}
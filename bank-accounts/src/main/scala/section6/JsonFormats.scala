package com.manel.bank_accounts.section6

import spray.json.DefaultJsonProtocol

// case class Account(accountId: String)

case class WireTransferRequest(from: String,to: String, amount: Int)

object JsonFormats  {
  import DefaultJsonProtocol._

  implicit val wireTransferRequestFormatJsonFormat = jsonFormat3(WireTransferRequest)
}

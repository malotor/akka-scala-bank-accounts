package com.manel.bank_accounts.section6

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route

import scala.concurrent.Future
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.util.Timeout
import scala.collection.mutable.HashMap

import com.manel.bank_accounts.section5.PremiumSupervisor

class Routes(val supervison: ActorRef, val system: ActorSystem) {
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import JsonFormats._
  import PremiumSupervisor.MakeWireTransfer

  private implicit val timeout = Timeout.create(system.settings.config.getDuration("bank-accounts.routes.ask-timeout"))

  val userRoutes: Route =
    pathPrefix("transfers") {
      concat(
        pathEnd {
          concat(
            get {
              complete(StatusCodes.OK)
            },
            post {
              entity(as[WireTransferRequest]) { transfer =>
                supervison ! MakeWireTransfer(transfer.from, transfer.to, transfer.amount)

                complete(StatusCodes.OK -> transfer)
              }
            })
        }
      )
    }
}

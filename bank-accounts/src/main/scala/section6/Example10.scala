package com.manel.bank_accounts.section6

import akka.actor.ActorSystem
import akka.actor.ActorRef
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route

import scala.util.Failure
import scala.util.Success

import scala.io.StdIn
import scala.collection.mutable.HashMap

import com.manel.bank_accounts.section5.{ AdvancedAccount, PremiumSupervisor }
import com.manel.bank_accounts.CommonApp

object Example10 {

  def main(args: Array[String]): Unit = {
    import AdvancedAccount._

    val system = ActorSystem("Bank-Accounts")

    var accounts = Vector.tabulate(3) { n => {
        val accountNumber = s"000${n+1}-USD"
        system.actorOf(AdvancedAccount.props(accountNumber), accountNumber)
      }
    }
    accounts.foreach(account =>
      account ! Operation(20000, CR)
    )
    val supervisor = system.actorOf(PremiumSupervisor.props, "supervisor")

    val routes = new Routes(supervisor, system)
    startHttpServer(routes.userRoutes)(system)
  }

  private def startHttpServer(routes: Route)(implicit system: ActorSystem): Unit = {
    implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

    val futureBinding = Http().newServerAt("localhost", 8080).bind(routes)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info("Server online at http://{}:{}/", address.getHostString, address.getPort)
      case Failure(ex) =>
        system.log.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
    StdIn.readLine() // let it run until user presses return
    futureBinding
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}

package com.manel.bank_accounts

import akka.actor.Actor
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
trait CommonApp {
  def printLog(txt: String): Unit = {
    val thread = Thread.currentThread.getName
    val pattern = DateTimeFormatter.ofPattern("m:s.SSS")
    val date = LocalDateTime.now.format(pattern)

    println(f"[${date}][$thread] $txt")
  }
}

trait CommonActor extends Actor{
  def printLog(txt: String): Unit = {
    val thread = Thread.currentThread.getName
    val pattern = DateTimeFormatter.ofPattern("m:s.SSS")
    val date = LocalDateTime.now.format(pattern)
    val actorName = self.path.name
    println(f"[${date}][${actorName}%15s] $txt")
  }

  def printFullLog(txt: String): Unit = {
    val thread = Thread.currentThread.getName
    val pattern = DateTimeFormatter.ofPattern("m:s.SSS")
    val date = LocalDateTime.now.format(pattern)
    val actorName = self.path.name
    println(f"[${date}][${thread}%30s][$actorName%15s] $txt")
  }
}
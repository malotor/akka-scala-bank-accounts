package com.manel.bank_accounts.section3

import akka.actor.{ Actor, ActorRef, ActorSystem, Props, FSM}
import com.manel.bank_accounts.CommonActor

object AdvancedAccount {
	// Account States
	sealed trait State
	case object Empty extends State
	case object Active extends State

	// Account Data
	sealed trait Data {
		val amount: Int
	}

	case object ZeroBalance extends Data {
		override val amount: Int = 0
	}
	case class Balance(override val amount: Int) extends Data
	
	// Transaction Types
	sealed trait TransactionType
	case object CR extends TransactionType
	case object DR extends TransactionType
	
	// Commands
	case class Operation(amount: Int, `type`: TransactionType)
	
	// Domain Events
	sealed trait DomainEvent
	case class AcceptedTransaction(account: ActorRef, amount: Float,`type`: TransactionType, newBalance: Int) extends DomainEvent
	case class RejectedTransaction(account: ActorRef, amount: Float,`type`: TransactionType, reason: String, currentBalance: Int) extends DomainEvent

	// Constructors
	def props(accountId: String): Props = Props(new AdvancedAccount(accountId, 0))

	def props(accountId: String, initialBalance: Int): Props = Props(new AdvancedAccount(accountId, initialBalance))
}

class AdvancedAccount(val accountId: String, val initialBalance: Int = 0) extends FSM[AdvancedAccount.State, AdvancedAccount.Data] with CommonActor {
	import AdvancedAccount._
	
	if (initialBalance > 0) startWith(Active, Balance(initialBalance))
	else startWith(Empty, ZeroBalance)

	when(Empty){
		case Event(Operation(amount, CR), _) =>
			goto(Active) using Balance(amount) replying AcceptedTransaction(self, amount, CR, amount)
		case Event(Operation(amount, DR), _) =>
			stay replying RejectedTransaction(self, amount, DR, "Balance is Zero", 0)
	}

	when(Active){
		case Event(Operation(amount, CR), balance) =>
			// Take some time to peform the operation
			Thread.sleep(50)
			val newBalance = balance.amount + amount
			stay using Balance(newBalance) replying AcceptedTransaction(self, amount, CR, newBalance)
		case Event(Operation(amount, DR), balance) =>
			// Take some time to peform the operation
			Thread.sleep(50)
			val newBalance = balance.amount - amount
			if(newBalance > 0){
				stay using Balance(newBalance) replying AcceptedTransaction(self, amount, DR, newBalance)
			}
			else if(newBalance == 0){
				goto(Empty) replying AcceptedTransaction(self, amount, DR, newBalance)
			}
			else {
				stay replying RejectedTransaction(self, amount, DR, "Balance doesn't cover this operation", balance.amount)
			}
	}
}
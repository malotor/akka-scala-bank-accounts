package com.manel.bank_accounts.section3

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.CommonApp

object Example6 extends App with CommonApp {
  import SeniorSupervisor._
  import SeniorOperator.Transaction

  val system = ActorSystem("Bank-Accounts")

  // Create an account with initial balance
  val account = system.actorOf(AdvancedAccount.props("0001-USD", 0), name = "0001-USD")
  
  val supervisor = system.actorOf(SeniorSupervisor.props, "supervisor")

  supervisor ! Transaction(account, 1000)
  supervisor ! Transaction(account, 500)
  supervisor ! Transaction(account, 400)
  supervisor ! Transaction(account, -1600)
  Thread.sleep(10000)
  supervisor ! Transaction(account, 400)
  supervisor ! Transaction(account, -600)
  supervisor ! Transaction(account, -250)
  supervisor ! Transaction(account, 100)
  
  Thread.sleep(10000)
  system.terminate()
}
package com.manel.bank_accounts.section3

import akka.actor.{ Actor, Props, ActorRef }
import com.manel.bank_accounts.CommonActor

object SeniorOperator {
  trait Commands
  case class Transaction(account: ActorRef, amount: Int) extends Commands

  def props = Props[SeniorOperator]
}

class SeniorOperator extends Actor with CommonActor {
  import SeniorOperator._
  import AdvancedAccount._

  def receive = {
    case Transaction(account, amount) if amount > 0 => {
      account ! Operation(amount, CR)
    }
    case Transaction(account, amount) => {
      account ! Operation(amount.abs, DR)
    }
    case AcceptedTransaction(account, amount, transactionType, newBalance) => {
      printLog(s"$amount $transactionType operation accepted. New balance is ${newBalance}")
    }
    case RejectedTransaction(account, amount, transactionType, reason, currentBalance) => {
      printLog(s"$amount $transactionType operation rejected. $reason. Current balance is ${currentBalance}")
    }
  }
}
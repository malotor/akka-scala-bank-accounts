package com.manel.bank_accounts.section3

import akka.actor.{ Actor, Props, ActorRef, Terminated, ActorLogging }
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, SmallestMailboxRoutingLogic, Router }
import com.manel.bank_accounts.CommonActor

object SeniorSupervisor {
  def props = Props[SeniorSupervisor]
}

class SeniorSupervisor extends Actor with CommonActor {
  import SeniorSupervisor._
  import SeniorOperator._

  var router = {
    val operators = Vector.tabulate(3) { n => {
        val operator = createNewOperator(n)
        ActorRefRoutee(operator)
      }
    }
    Router(SmallestMailboxRoutingLogic(), operators)
  }

  def receive = {
    // Send work to the operator
    case transaction: Transaction =>
      printLog(s"Routing ${transaction}")
      router.route(transaction, sender())
  }

  def createNewOperator(n: Int) = {
    context.actorOf(SeniorOperator.props, s"operator_$n")
  }
}
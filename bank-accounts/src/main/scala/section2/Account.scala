package com.manel.bank_accounts.section2

import akka.actor.{ Actor, ActorRef, ActorSystem, Props, FSM}
import com.manel.bank_accounts.CommonActor

// Finite State Machine
// https://en.wikipedia.org/wiki/Finite-state_machine
// State pattern
// https://en.wikipedia.org/wiki/State_pattern
object Account {
	// 1 - Account States
	sealed trait State
	case object Empty extends State
	case object Active extends State

	// 2 - Account Data
	sealed trait Data {
		val amount: Int
	}

	case object ZeroBalance extends Data {
		override val amount: Int = 0
	}
	case class Balance(override val amount: Int) extends Data

	// Transaction Types
	sealed trait TransactionType
	case object CR extends TransactionType
	case object DR extends TransactionType

	// Commands
	case class Operation(amount: Int, `type`: TransactionType)
}

class Account extends FSM[Account.State, Account.Data] with CommonActor {
	import Account._

	startWith(Empty, ZeroBalance)

	// 3 - State transitions
	when(Empty){
		// case Event(Operation(amount, CR), currentBalance) =>
		case Event(Operation(amount, CR), _) =>
			printLog(s"Hi, It's your first Credit Operation.")
			printLog(s"Your new balance is ${amount}")
			goto(Active) using Balance(amount)
		case Event(Operation(amount, DR), _) =>
			printLog(s"Sorry your account has zero balance.")
			stay
	}

	when(Active){
		case Event(Operation(amount, CR), currentBalance) =>
			val newBalance = currentBalance.amount + amount
			printLog(s"Your new balance is ${newBalance}")
			stay using Balance(newBalance)
		case Event(Operation(amount, DR), currentBalance) =>
			val newBalance = currentBalance.amount - amount
			if(newBalance > 0){
				printLog(s"Your new balance is ${newBalance}")
				stay using Balance(newBalance)
			}
			else if(newBalance == 0){
				printLog(s"Your new balance is ${newBalance}")
				goto(Empty)
			}
			else {
				printLog("Balance doesn't cover this operation.")
				stay
			}
	}
}
package com.manel.bank_accounts.section2

import akka.actor.{ Actor, Props, ActorRef }
import com.manel.bank_accounts.CommonActor

object LazyOperator {
  trait Commands
  case class Transaction(account: ActorRef, amount: Int) extends Commands

  def props = Props[LazyOperator]

  def props(delay: Int): Props = Props(new LazyOperator(delay))
}

class LazyOperator(delay: Int) extends Actor with CommonActor {
  import LazyOperator._
  import Account._

  def receive = {
    case Transaction(account, amount) if amount > 0 => {
      goToSleep
      printLog(s"Sending CR $amount")
      account ! Operation(amount, CR)
    }
    case Transaction(account, amount) => {
      goToSleep
      printLog(s"Sending DR $amount")
      account ! Operation(amount.abs, DR)
    }
  }

  private def goToSleep = {
    val r = new scala.util.Random
    val sleepTime = 5 * delay * 1000
    printLog(s"Sleeping ${sleepTime/1000} seconds .... zzzzzzz")
    Thread.sleep(sleepTime)
  }
}
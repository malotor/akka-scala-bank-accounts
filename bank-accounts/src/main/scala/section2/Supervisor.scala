package com.manel.bank_accounts.section2

import akka.actor.{ Actor, Props, ActorRef, Terminated, ActorLogging }
import akka.routing.{ ActorRefRoutee, RoundRobinRoutingLogic, SmallestMailboxRoutingLogic, Router }
import com.manel.bank_accounts.CommonActor

object Supervisor {
  def props = Props[Supervisor]
}

class Supervisor extends Actor with CommonActor {
  import Supervisor._
  import LazyOperator._

  // Create a router
  var router: Router = {
    // Creates 3 operators
    val operators = Vector.tabulate(3) { n => {
        val operator = createNewOperator(n+1)
        ActorRefRoutee(operator)
      }
    }
    // Set routing strategy
    // Router(RoundRobinRoutingLogic(), operators)
    Router(SmallestMailboxRoutingLogic(), operators)
  }

  def receive = {
    // Send work to the operator
    case transaction: Transaction =>
      printLog(s"Routing ${transaction}")
      router.route(transaction, sender())
  }

  def createNewOperator(n: Int) = {
    context.actorOf(LazyOperator.props(n), s"operator_$n")
  }
}
package com.manel.bank_accounts.section2

import akka.actor.{ActorRef, ActorSystem, Props}
import com.manel.bank_accounts.CommonApp

object Example5 extends App with CommonApp {
  import Supervisor._
  import LazyOperator.Transaction

  val system = ActorSystem("Bank-Accounts")

  val account = system.actorOf(Props[Account], "0001-USD")

  val supervisor = system.actorOf(Supervisor.props, "supervisor")

  supervisor ! Transaction(account, 100)
  supervisor ! Transaction(account, 200)
  supervisor ! Transaction(account, 300)

  Thread.sleep(7000)

  supervisor ! Transaction(account, 400)

  Thread.sleep(7000)

  supervisor ! Transaction(account, 500)

  Thread.sleep(30000)
  system.terminate()
}
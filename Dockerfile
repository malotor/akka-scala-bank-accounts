FROM hseeberger/scala-sbt:graalvm-ce-20.0.0-java11_1.4.2_2.11.12

# For a Alpine Linux version, comment above and uncomment below:
# FROM 1science/sbt
EXPOSE 8080
RUN mkdir -p /exampleapp
RUN mkdir -p /exampleapp/out

WORKDIR /exampleapp

COPY . .
# Actor mode with Scala and Akka

## Set up
```
make build
make bash
```
## Compile and execute exercices
```
$ make bash
$ cd bank-accounts/
$ sbt
$ runMain com.manel.section1.Exercice1

```
